const express = require("express");
const cors = require("cors");
const { nanoid } = require("nanoid");var fs = require('fs')
const morgan = require('morgan')
const path = require('path')

const app = express();

const corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

// create a write stream (in append mode)
var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })
 
// setup the logger
app.use(morgan('combined', { stream: accessLogStream }))

// database
const db = require("./app/models");
//const email = require("./app/services/email.service");
const Role = db.role;

const Author = db.author;
const Book = db.book;
const Genre = db.genre;
//const { nanoid } = require("nanoid");
//const idLength = 13;

//db.sequelize.sync();
// force: true will drop the table if it already exists

(async () => {
  await db.sequelize.sync({ force: true });
  console.log('Drop and Resync Database with { force: true }');
  initial();
})();

// simple route

app.get("/", (req, res) => {
  res.json({ message: "Welcome to 'Livres en Folies' API." });
});

// routes
require('./app/routes/auth.routes')(app);
require('./app/routes/user.routes')(app);

require('./app/routes/author.routes')(app);
require('./app/routes/book.routes')(app);
require('./app/routes/genre.routes')(app);
require('./app/routes/basket.routes')(app);
require('./app/routes/order.routes')(app);


// set port, listen for requests
const PORT = process.env.PORT || 4000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

function initial() 
{
   //======= Default Roles ========
    Role.create({
      id: 1,
      name: "user"
    });
  
    Role.create({
      id: 2,
      name: "admin"
    });

  //======= Genres =========
      //Roman 
      Genre.create({
        name: 'Roman'
      });

      //Jeunesse
      Genre.create({
        name: 'Jeunesse'
      });
      
      //Informatique
      Genre.create({
        name: 'Informatique'
      });
      
      //Comédie
      Genre.create({
        name: 'Comédie'
      });
      
      //Drame
      Genre.create({
        name: 'Drame'
      });
      
      //Historique
      Genre.create({
        name: 'Historique'
      });

  //======= Default Authors ========
      // Author 1
      Author.create({ 
        name: 'Mark Lutz', 
        email: 'mark.lutz@livresenfolies.ht',
        phone: '+(509) 1234-5678',
        website: 'www.lef.ht'
      });
      // Author 2
      Author.create({ 
        name: 'Gary Victor', 
        email: 'gary.victor@livresenfolies.ht',
        phone: '+(509) 1234-5678',
        website: 'www.lef.ht'
      });
      // Author 3
      Author.create({ 
        name: 'Joseph Anténor Firmin', 
        email: 'Joseph.Anténor Firmin@livresenfolies.ht',
        phone: '+(509) 1234-5678',
        website: 'www.lef.ht'
      });
      // Author 4
      Author.create({ 
        name: 'Jeff Destinvil', 
        email: 'Jeff.Destinvil@livresenfolies.ht',
        phone: '+(509) 1234-5678',
        website: 'www.lef.ht'
      });
      // Author 5
      Author.create({ 
        name: 'Pierre Michelot Jean Claude Victor', 
        email: 'Pierre.Michelot.Jean.Claude@livresenfolies.ht',
        phone: '+(509) 1234-5678',
        website: 'www.lef.ht'
      });
      // Author 6
      Author.create({ 
        name: 'Iléus Papillon', 
        email: 'Iléus.Papillon@livresenfolies.ht',
        phone: '+(509) 1234-5678',
        website: 'www.lef.ht'
      });
       // Author 7
      Author.create({ 
        name: 'Francketienne', 
        email: 'francketienne@livresenfolies.ht',
        phone: '+(509) 1234-5678',
        website: 'www.lef.ht'
      });
      // Book 1 | Author 1
      Book.create({
          isbn: "978-1449355735",
          title: "Learning Python, 5th Edition Fifth Edition",
          summary: "Price: 649.00 USD & FREE shipping",
          edition: "Fifth",
          publisher: "O'Reilly Media",
          year: "2013",
          price: 33.86,
          currency: "USD",
          image: "https://images-na.ssl-images-amazon.com/images/I/41nRJEUxePS._SX379_BO1,204,203,200_.jpg",
          authorid:1,
          genreId: 4
 
      });
      // Book 2
      Book.create({
        isbn: "978-1449355739",
        title: "Learning Python, 5th Edition Fifth Edition",
        summary: "Price: 649.00 USD & FREE shipping",
        edition: "Fifth",
        publisher: "O'Reilly Media",
        year: "2013",
        price: 33.86,
        currency: "USD",
        image: "https://images-na.ssl-images-amazon.com/images/I/41nRJEUxePS._SX379_BO1,204,203,200_.jpg",
        authorid: 5,
        genreId: 4

    });
    
}