module.exports = (sequelize, Sequelize) => {
	const OrderLine = sequelize.define('orderLines', {
	id: {
		type: Sequelize.INTEGER,
			allowNull: false,
			autoIncrement: true,
			primaryKey: true
		},
    quantity: {
		  type: Sequelize.INTEGER
	  },
	total: {
		  type: Sequelize.DECIMAL(19, 2) 
	  }
	});
	
	return OrderLine;
}