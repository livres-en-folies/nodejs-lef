module.exports = (sequelize, Sequelize) => {
	const Book = sequelize.define('books', {
		id: {
			type: Sequelize.INTEGER,
			allowNull: false,
			autoIncrement: true,
			primaryKey: true
		},
	  isbn: {
		  type: Sequelize.STRING,//(13),
		  unique: true,
		  defaultValue: Sequelize.UUIDV1//,	 primaryKey: true
	  },
	  title: {
		  type: Sequelize.STRING,
		  allowNull: false
	  },
	  summary: {
		  type: Sequelize.STRING,
		  allowNull: false
	  },
	  edition: {
		  type: Sequelize.STRING,
		  allowNull: false
	  },
	  publisher: {
		  type: Sequelize.STRING,
		  allowNull: false
	  },
	  year: {
		  type: Sequelize.INTEGER,
		  allowNull: false
	  },
     price: {
		  type: Sequelize.DOUBLE,
		  allowNull: false
	  },
	  currency: {
		  type: Sequelize.STRING,
		  allowNull: false
	  },
	  image: {
		  type: Sequelize.STRING
	  }
	});
	
	return Book;
}

