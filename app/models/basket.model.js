module.exports = (sequelize, Sequelize) => {
	const Basket = sequelize.define('basket', {
       
        Id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },

        userid: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        bookid: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        quantity: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        subTotal: {
            type: Sequelize.DECIMAL(19, 2) ,
            allowNull: false
        }
    }
);

	return Basket;
}