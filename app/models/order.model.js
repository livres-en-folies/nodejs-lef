module.exports = (sequelize, Sequelize) => {
	const Order = sequelize.define('orders', {
	id: {
		type: Sequelize.INTEGER,
			allowNull: false,
			autoIncrement: true,
			primaryKey: true
	},
    status: {
        type: Sequelize.STRING,
		allowNull: false
    },
	totalAmount: {
		  type: Sequelize.DECIMAL(19, 2) ,
		  allowNull: false
	},
	confirmation: {
		type: Sequelize.INTEGER,
			allowNull: false,
			unique: true
	
	  }
	}
	
	);
	
	return Order;
}