const config = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(
  config.DB,
  config.USER,
  config.PASSWORD,
  {
    host: config.HOST,
    dialect: config.dialect,
    operatorsAliases: false,

    pool: {
      max: config.pool.max,
      min: config.pool.min,
      acquire: config.pool.acquire,
      idle: config.pool.idle
    }
  }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

//############# Livres en Folies Entities ###############
db.genre = require("../models/genre.model.js")(sequelize, Sequelize);
db.author = require("../models/author.model.js")(sequelize, Sequelize);
db.book = require("../models/book.model.js")(sequelize, Sequelize);

// 1 Book -> 1 Genre
db.genre.hasMany(db.book);
db.book.belongsTo(db.genre);

// 1 Author -> * Books
db.author.hasMany(db.book, {foreignKey: 'authorid',allowNull: false, sourceKey: 'id'});
db.book.belongsTo(db.author, {foreignKey: 'authorid', allowNull: false,targetKey: 'id'});
// End Authors ###############


//############# Users ################# 
db.user = require("../models/user.model.js")(sequelize, Sequelize);
db.role = require("../models/role.model.js")(sequelize, Sequelize);
db.refreshToken = require("../models/refreshToken.model.js")(sequelize, Sequelize);

    // Users associations
    db.role.belongsToMany(db.user, {
      through: "user_roles",
      foreignKey: "roleId",
      otherKey: "userId"
    });

    db.user.belongsToMany(db.role, {
      through: "user_roles",
      foreignKey: "userId",
      otherKey: "roleId"
    });

    db.refreshToken.belongsTo(db.user, {
      foreignKey: 'userId', targetKey: 'id'
    });
    db.user.hasOne(db.refreshToken, {
      foreignKey: 'userId', targetKey: 'id'
    });

    db.ROLES = ["user", "admin"];
// End Users ################# 


//############# Order ###############
db.order = require("../models/order.model.js")(sequelize, Sequelize);
db.OrderLine = require("./orderLines.model.js")(sequelize, Sequelize);
db.basket = require("./basket.model.js")(sequelize, Sequelize);
    //associations
    db.book.hasMany(db.basket, {foreignKey: 'bookid', sourceKey: 'id'});
    db.basket.belongsTo(db.book, {foreignKey: 'bookid', targetKey: 'id'});
   
    // 1 user -> * items
    db.user.hasMany(db.basket, {foreignKey: 'userid', sourceKey: 'id'});
    db.basket.belongsTo(db.user, {foreignKey: 'userid', targetKey: 'id'});

    // 1 user -> * order
    db.user.hasMany(db.order, {foreignKey: 'userid', sourceKey: 'id'});
    db.order.belongsTo(db.user, {foreignKey: 'userid', targetKey: 'id'});

    // 1 order -> * line_item
    db.order.hasMany(db.OrderLine, {foreignKey: 'orderid', sourceKey: 'id'});
    db.OrderLine.belongsTo(db.order, {foreignKey: 'orderid', targetKey: 'id'});
   
    //1 orderLine -> 1 book
    db.OrderLine.belongsTo(db.book, {foreignKey: 'bookid', targetKey: 'id'});
    db.book.hasOne(db.OrderLine, {foreignKey: 'bookid', targetKey: 'id'});
module.exports = db;
