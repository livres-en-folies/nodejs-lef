module.exports = (sequelize, Sequelize) => {
	const Author = sequelize.define('authors', {
	id: {
			type: Sequelize.INTEGER,
			allowNull: false,
			autoIncrement: true,
			primaryKey: true
		},
	name: {
		  type: Sequelize.STRING,
		  allowNull: false
	  },
	  email: {
		  type: Sequelize.STRING,
		  unique: true,
		  allowNull: false
	  },
	  phone: {
		  type: Sequelize.STRING,
		  allowNull: false
	  },
	  website: {
		  type: Sequelize.STRING
	  }
	});
	
	return Author;
}