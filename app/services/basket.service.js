const db = require("../models");
const Book = db.book;
const Baskets = db.basket;
const Users = db.user;
// Post a Book to basket
exports.addBookToShoppingBasket = (req, res) => {
	// Add Book to basket
	Baskets.create({  
		quantity: req.body.quantity,
		bookid: req.body.bookId,
		userid: req.body.userId,
		subTotal: req.body.total
	}).then(basket => {
		// Send item to client
		res.send(basket);
	}).catch(err => {
		res.status(500).send("Error -> " + err);
	})
};

exports.loadShoppingBasketByUserId = (req, res) => {
	Baskets.findOne({where: { userid: req.params.userId},
		attributes: [['id', 'basketId'], 'quantity','subTotal','userId'],
		include: [	{
					model: Book,
					where: { id: db.Sequelize.col('book.id') },
					attributes: [['id', 'bookId'], 'isbn','title', 'summary', 'edition', 'publisher', 'year', 'price', 'currency', 'image'],
				},
				{
					model: Users,
					attributes: [['id','userId'],'username',['name','full_name'],'email'],
				}
	]
	}).then(basket => {
		res.send(basket);
}).catch(err => {
	res.status(500).send("Error -> " + err);
})
};

exports.findAll = (req, res) => {
	Baskets.findAll({
		attributes: [['id', 'basketId'], 'quantity','subTotal','userId'],
		include: [	{
					model: Book,
					where: { id: db.Sequelize.col('book.id') },
					attributes: [['id', 'bookId'], 'isbn','title', 'summary', 'edition', 'publisher', 'year', 'price', 'currency', 'image'],
				}, {
					model: Users,
						attributes: [['id','userId'],'username',['name','full_name'],'email'],
					}
				
		]
	}).then(basket => {
		res.send(basket);
}).catch(err => {
	res.status(500).send("Error -> " + err);
})
};
 
// Delete a Item by Id
exports.delete = (req, res) => {
	const id = req.params.basketId;
	Baskets.destroy({
				where: { Id: id }
			}).then(() => {
				res.status(200).send('Item has been deleted!');
			}).catch(err => {
				res.status(500).send("Fail to delete!");
			});
};


exports.test=(req,res) =>{
	const { QueryTypes } = require('sequelize');
	sequelize.query(
		"INSERT INTO clients (name) values ('myClient')",
		{ type: sequelize.QueryTypes.INSERT }
	).then(function (clientInsertId) {
		console.log(clientInsertId);
	});
};