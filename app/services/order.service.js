const axios = require('axios').default;
const db = require("../models");
const Orders = db.order;
const OrderLines = db.orderLines;
const Users = db.user;
const Baskets = db.basket;

exports.orderPayment = (req, res) => {
   const _userId = req.params.userId;
   const _numeroCompte = req.body.numeroCompte;
   const  _idOperation = req.body.idOperation;
   const  _montant = req.body.montant;
   const  _token = req.body.token;
   const _status = "Completed"
     
  // Bank API for a payment.
    axios.defaults.baseURL = 'http://209.126.85.241:8080';
    //axios.defaults.baseURL = 'http://livresenfolies.online:8080';
    //axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
    axios.defaults.headers.post['Content-Type'] = 'application/json';
    //axios.defaults.timeout =100000, // Wait for 5 seconds
    axios.post('/bank/webresources/compte/end_transaction', {
        numeroCompte: _numeroCompte,
        idOperation: _idOperation,
        montant: _montant,
        token: _token
      })
      .then(function (response) {
      if (response.data.code == 200){
            // create order
            Orders.create({  
                status: _status,
                totalAmount: _montant,
                userid: _userId,
                confirmation:_idOperation
            }).then(order => {
               // Baskets.findAll()

                // delete items form basket by userID
                Baskets.destroy({
                    where: { userid: _userId }
                }).then(() => {
                    //res.status(200).send('Item has been deleted!');
                    // Send order to client
                     res.send(order);
                });

            }).catch(err => {
                res.status(500).send("Error -> " + err);
            })
        
      }else
      {
          // Send error to client
        console.log(response.data);
        res.status(400).send(response.data);
      }
       
      })
      .catch(function (error) {
        console.log(error);
        res.status(500).send(error);
      });
  };

exports.findAllOrders = (req, res) => {

    Orders.findAll({
        attributes: [['id', 'orderId'], ['totalAmount','amount'],"userid",['confirmation','no_confirmation']],
        include: [{
			model: Users,
                attributes: [['id','userId'],'username',['name','full_name'],'email'],
            }
        ]
        }).then(order => {
            res.send(order);
    }).catch(err => {
        res.status(500).send("Error -> " + err);
    })
};
 
exports.findAllOrdersByUser = (req, res) => { 
    Orders.findAll({where: { userid: req.params.userId},
            attributes: [['id', 'orderId'], ['totalAmount','amount'],"userid",['confirmation','no_confirmation']],
        
        }).then(order => {
            res.send(order);
    }).catch(err => {
        res.status(500).send("Error -> " + err);
    })
 
};
   
exports.findAllOrderLinesByUser = (req, res) => { 
    console.log("findAllOrderLinesByUser");
    console.log(req.params.userId);
    console.log(req.body);
   /* OrderLines.findAll({where: { userid: req.params.userId},
           // attributes: [['id', 'orderId'], ['totalAmount','amount'],"userid",['confirmation','no_confirmation']],
        
        }).then(OrderLines => {
            res.send(OrderLines);
    }).catch(err => {
        res.status(500).send("Error -> " + err);
    })
 */
};
   