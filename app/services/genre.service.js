const db = require("../models");
const Genres = db.genre;
const Book = db.book;

// Fetch all Genres
exports.findAll = (req, res) => {
	Genres.findAll({
		attributes: [['id', 'genreId'], 'name'],
	}).then(genres => {
		// Send all Genres to Client
		res.send(genres);
	}).catch(err => {
		res.status(500).send("Error -> " + err);
	})
};

// Fetch all Genres
exports.findAllBooksByGenreId = (req, res) => {
	Genres.findByPk(req.params.genreId,
		{
			attributes: [['id', 'genreId'], 'name'],
			include: [{
				model: Book,
				attributes: [['id', 'bookId'], 'isbn','title', 'summary', 'edition', 'publisher', 'year', 'price', 'currency', 'image'],
			},]
		}).then(genre => {
		res.send(genre);
	}).catch(err => {
		res.status(500).send("Error -> " + err);
	})
};