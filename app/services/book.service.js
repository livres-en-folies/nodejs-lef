const db = require("../models");
const Books = db.book;
const Author = db.author;
const Genre = db.genre;


// Post a Book
exports.create = (req, res) => {
	 // Validate request
	 if (!req.body.title) {
		res.status(400).send({
		  message: "Content can not be empty!"
		});
		return;
	  }	
	// Save Book to MySQL database
	Books.create({  
		isbn: req.body.isbn,
		title: req.body.title,
		summary: req.body.summary,
		edition: req.body.edition,
		publisher: req.body.publisher,
		year: req.body.year,
		price: req.body.price,
		currency: req.body.currency,
		image: req.body.image,
		genreId: req.body.genreId,
		authorid: req.body.authorId
	}).then(book => {
		
       	// Send created book to client
		res.send(book);
	}).catch(err => {
		res.status(500).send("Error -> " + err);
	})
};
 
// Fetch all Books
exports.findAll = (req, res) => {
	Books.findAll({
		attributes: [['id', 'bookId'], 'isbn','title', 'summary', 'edition', 'publisher', 'year', 'price', 'currency', 'image'],
		include: [{
			model: Genre,
			where: { id: db.Sequelize.col('genre.Id') },
			attributes: [['id', 'genreId'],'name'],
		},{
			model: Author,
			where: { id: db.Sequelize.col('author.id') },
			attributes:[['id', 'authorId'],'name', 'email','phone','website'],
		}]
	}).then(books => {
		// Send all Books to Client
		res.send(books);
}).catch(err => {
	res.status(500).send("Error -> " + err);
})
};
 
// Find a Book by Id
exports.findById = (req, res) => {	
	Books.findByPk(req.params.bookId,
		{
			attributes: [['id', 'bookId'], 'isbn','title', 'summary', 'edition', 'publisher', 'year', 'price', 'currency', 'image'],
			include: [{
				model: Genre,
				where: { id: db.Sequelize.col('genre.Id') },
				attributes: [['id', 'genreId'], 'name'],
			},{
				model: Author,
				where: { id: db.Sequelize.col('author.id') },
				attributes: [['id', 'authorId'], 'name', 'email','phone','website'],
			}]
		}).then(book => {
		res.send(book);
	}).catch(err => {
		res.status(500).send("Error -> " + err);
	})
};

// Find  Books by GenreId
exports.findByGenreId = (req, res) => {	
	Books.findAll({where: { genreId: req.params.genreId},
			attributes: [['id', 'bookId'], 'isbn','title', 'summary', 'edition', 'publisher', 'year', 'price', 'currency', 'image'],
			include: [{
				model: Genre,
				where: { id: db.Sequelize.col('genre.Id') },
				attributes: [['id', 'genreId'], 'name'],
			},{
				model: Author,
				where: { id: db.Sequelize.col('author.id') },
				attributes: [['id', 'authorId'], 'name', 'email','phone','website'],
			}]
		}).then(book => {
		res.send(book);
	}).catch(err => {
		res.status(500).send("Error -> " + err);
	})
};

// Find  Books by AuthorId
exports.findByAuthorId = (req, res) => {	
	Books.findAll({where: { authorid: req.params.authorId},
		attributes: [['id', 'bookId'], 'isbn','title', 'summary', 'edition', 'publisher', 'year', 'price', 'currency', 'image'],
			include: [{
				model: Genre,
				where: { id: db.Sequelize.col('genre.Id') },
				attributes: [['id', 'genreId'], 'name'],
			},{
				model: Author,
				where: { id: db.Sequelize.col('author.id') },
				attributes: [['id', 'authorId'], 'name', 'email','phone','website'],
			}]
		}).then(book => {
		res.send(book);
	}).catch(err => {
		res.status(500).send("Error -> " + err);
	})
};


// Update a Book by Id
exports.update = (req, res) => {
	var book = req.body;
	const id = req.params.bookId;
	Books.update({ 
					isbn: req.body.isbn,
					title: req.body.title,
					summary: req.body.summary,
					edition: req.body.edition,
					publisher: req.body.publisher,
					year: req.body.year,
					price: req.body.price,
					currency: req.body.currency,
					image: req.body.image,
					authorid: req.body.authorId,
					genreId: req.body.genreId
				}, 
				{ 
					where: {
						id: req.params.bookId
					} 
				})
				.then(() => {
						res.status(200).send(book);
				   }).catch(err => {
						res.status(500).send("Error -> " + err);
				   })
};
 
// Delete a Book by Id
exports.delete = (req, res) => {
	const id = req.params.bookId;
	Books.destroy({
				where: { id: id }
			}).then(() => {
				res.status(200).send('Book has been deleted!');
			}).catch(err => {
				res.status(500).send("Fail to delete!");
			});
};

