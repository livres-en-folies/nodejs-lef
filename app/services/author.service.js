
const db = require("../models");
const Author = db.author;
const Book = db.book;
const Genre = db.genre;

// Fetch all Auteurs include Livres
exports.findAllAuthorWithBooks = (req, res) => {
	Author.findAll({
		attributes: [['id', 'authorId'], 'name', 'email', 'phone','website'],
		include: [
					{
						model: Book,
						attributes: [['id', 'bookId'],'isbn', 'title', 'summary', 'edition', 'publisher', 'year', 'price', 'currency', 'image']
					},
		]
	
	}).then(authors => {
	   res.send(authors);
	});
};

// Post a Author
exports.createAuthor = (req, res) => {	
	// Validate request
	if (!req.body.name) {
		res.status(400).send({
		  message: "Content can not be empty!"
		});
		return;
	  }
	// Save Author to database
	Author.create({  
		name: req.body.name,
		email: req.body.email,
		phone: req.body.phone,
		website: req.body.website
	}).then(author => {
		// Send created author to client
		res.send(author);
	}).catch(err => {
		res.status(500).send("Error -> " + err);
	})
};
 
// Fetch all Author
exports.findAll = (req, res) => {
	Author.findAll({
		attributes: [['id', 'authorId'], 'name', 'email', 'phone','website']        //,
	}).then(authors => {
		// Send all Books to Client
		res.send(authors);
	}).catch(err => {
		res.status(500).send("Error -> " + err);
	})
};

// Find a Author by Id
exports.findOne = (req, res) => {	

	Author.findByPk(req.params.authorId,{
		attributes: [['id', 'authorId'], 'name', 'email', 'phone','website'] 
	}).then(author => {
		
		res.send(author);
	}).catch(err => {
		res.status(500).send("Error -> " + err);
	})
};

// Update a Author
exports.update = (req, res) => {
	var author = req.body;
	
	const id = req.params.authorId;
	Author.update({ 
					name: req.body.name,
					email: req.body.email,
					phone: req.body.phone,
					website: req.body.website

				}, 
				{ 
					where: {
						id: req.params.authorId
					} 
				})
				.then(() => {
						res.status(200).send(author);
				   }).catch(err => {
						res.status(500).send("Error -> " + err);
				   })
};
 
// Delete a Author by Id
exports.delete = (req, res) => {
	const id = req.params.authorId;
	Author.destroy({
				where: { id: id }
			}).then(() => {
				res.status(200).send('Author has been deleted!');
			}).catch(err => {
				res.status(500).send("Fail to delete!");
			});
};

