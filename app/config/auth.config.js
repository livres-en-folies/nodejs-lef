module.exports = {
  secret: "Livres-En-Folies-Secret-Key",
   jwtExpiration: 3600,         // 1 heure
   jwtRefreshExpiration: 86400, // 24 heures

  /*  En test*/
  //jwtExpiration: 60,          // 1 minute
  //jwtRefreshExpiration: 120,  // 2 minutes
};
