const { verifySignUp } = require("../middleware");
const controller = require("../services/auth.service");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });
  
  var router = require("express").Router();

  app.use('/api/auth', router);
  
  router.post("/signup",
    [
      verifySignUp.checkDuplicateUsernameOrEmail,
      verifySignUp.checkRolesExisted
    ],
    controller.signup
  );

  router.post("/signin", controller.signin);

  router.post("/refreshtoken", controller.refreshToken);
  
};
