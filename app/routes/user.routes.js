const { authJwt } = require("../middleware");
const userData = require("../services/user.service");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  var router = require("express").Router();

  app.use('/api/test', router);

  router.get("/user",
            [authJwt.verifyToken],
            userData.userBoard
  );
  
  router.get("/admin",
            [authJwt.verifyToken, authJwt.isAdmin],
            userData.adminBoard
  );
};
