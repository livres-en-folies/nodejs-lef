const { authJwt } = require("../middleware");
const genreData = require("../services/genre.service");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

var router = require("express").Router();

app.use('/api/genres', router);

// Retrieve all Genres
router.get('/', 
            [authJwt.verifyToken],       
            genreData.findAll);

// Retrieve a single Genre by Id
router.get('/:genreId',
            [authJwt.verifyToken],       
            genreData.findAllBooksByGenreId);

};
