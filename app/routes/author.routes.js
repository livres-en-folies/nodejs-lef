const { authJwt } = require("../middleware");
const authorData = require("../services/author.service");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });
 
var router = require("express").Router();

app.use('/api/authors', router);

// Create a new author
router.post('/',   
            [authJwt.verifyToken, authJwt.isAdmin],
            authorData.createAuthor);

// Retrieve all authors
router.get("/", 
            [authJwt.verifyToken], 
            authorData.findAll);

// Retrieve all authors
router.get("/books", 
          [authJwt.verifyToken], 
          authorData.findAllAuthorWithBooks);

// Retrieve a single author by Id
router.get('/:authorId', 
          [authJwt.verifyToken], 
          authorData.findOne);

// Update a author with Id
router.put('/:authorId', 
          [authJwt.verifyToken, authJwt.isAdmin],
          authorData.update);

// Delete a author with Id
router.delete('/:authorId',
              [authJwt.verifyToken, authJwt.isAdmin],
              authorData.delete);



};

