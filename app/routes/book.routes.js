const { authJwt } = require("../middleware");
const bookData = require("../services/book.service");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  var router = require("express").Router();

  app.use('/api/books', 
            router);

  // Create a new Book
  router.post('/', 
              [authJwt.verifyToken,authJwt.isAdmin],
              bookData.create);
 
  // Retrieve all Books
  router.get('/',
              [authJwt.verifyToken],
              bookData.findAll);

  // Retrieve a single Book by Id
  router.get('/:bookId',
              [authJwt.verifyToken],
              bookData.findById);

  // Retrieve Books by GenreId
  router.get('/genres/:genreId', 
              [authJwt.verifyToken],
              bookData.findByGenreId);

  // Retrieve  Books by AuthorId
  router.get('/authors/:authorId', 
              [authJwt.verifyToken],
              bookData.findByAuthorId);

  // Update a Book with Id
  router.put('/:bookId', 
              [authJwt.verifyToken,authJwt.isAdmin],
              bookData.update);

  // Delete a Book with Id
  app.delete('/api/books/:bookId', 
              [authJwt.verifyToken,authJwt.isAdmin],
              bookData.delete);

};
