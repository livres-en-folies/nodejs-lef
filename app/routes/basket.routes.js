const { authJwt } = require("../middleware");
const shoppingData = require("../services/basket.service");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

var router = require("express").Router();

app.use('/api/baskets', router);

// Add Books to Basket
router.post('/', 
            [authJwt.verifyToken],
            shoppingData.addBookToShoppingBasket);

// Fetch Books from Basket for a user
router.get('/:userId', 
            [authJwt.verifyToken],
            shoppingData.loadShoppingBasketByUserId);

// Fetch Books from Basket
router.get('/', 
            [authJwt.verifyToken],
            shoppingData.findAll);

// Delete an Item with BasketId
router.delete('/:basketId', 
              [authJwt.verifyToken],
              shoppingData.delete);

router.post('/test', 
              shoppingData.test);

};



