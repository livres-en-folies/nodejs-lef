const { authJwt } = require("../middleware");
const orderData = require("../services/order.service");

module.exports = function(app) {
    app.use(function(req, res, next) {
      res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
      );
      next();
    });

    var router = require("express").Router();

    app.use('/api/orders', router);

    router.post("/:userId/payment",
                [authJwt.verifyToken],
                orderData.orderPayment
    );

    router.get("/",
                [authJwt.verifyToken],
                orderData.findAllOrders
    );

    router.get("/users/:userId",
                [authJwt.verifyToken],
                orderData.findAllOrdersByUser
    );

    router.get("/users/:userId/items",
              //[authJwt.verifyToken],
              orderData.findAllOrderLinesByUser
    );


};